package eportfolio;

import GUI.ePortfolioGUI;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Daniel Rosentha;
 */
public class Eportfolio extends Application {
     // THIS WILL PERFORM SLIDESHOW READING AND WRITING
    
    //SlideShowFileManager fileManager = new SlideShowFileManager();

    // THIS HAS THE FULL USER INTERFACE AND ONCE IN EVENT
    // HANDLING MODE, BASICALLY IT BECOMES THE FOCAL
    // POINT, RUNNING THE UI AND EVERYTHING ELSE
    ePortfolioGUI ui = new ePortfolioGUI();

    @Override
    public void start(Stage primaryStage) throws Exception {
	
	    ui.startUI(primaryStage);
	
    }
    public static void main(String args[]){
        launch(args);
    }
    
}
