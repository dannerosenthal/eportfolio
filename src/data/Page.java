/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Daniel
 */
public class Page {
   String title;
   String pageFont;
   ObservableList<HTMLElement> elements;
   HTMLElement selectedElement;
   
   public Page(String initTitle, String initFont){
       elements = FXCollections.observableArrayList();
       title = initTitle;
       pageFont = initFont;
    }
    
    public void setTitle(String initTitle){
        title=initTitle;
    }
    public String getTitle(){
        return title;
    }
    public void setFont(String newFont){
        pageFont=newFont;
        
    }
    public String getFont(){
        return pageFont;
    }
    public ObservableList getElements(){
        return elements;
    }
    public HTMLElement getSelectedElement(){
        return selectedElement;
    }
    
    public void addElement(HTMLElement elementToAdd){
        elements.add(elementToAdd);
    }
    public void removeelement(){
        if (selectedElement!=null){
            elements.remove(selectedElement);
            selectedElement=null;
        }
    }
}
