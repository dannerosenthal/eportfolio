/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author Daniel
 */
public class Video extends HTMLElement{
    String source;
    public Video(String initFontStyle, String initSource) {
        super("video", initFontStyle);
        source = initSource;
    }
    public String getSource(){
        return source;
    }
    public void setSource(String initSource){
        source = initSource;
    }
}
