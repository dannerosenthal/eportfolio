/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Daniel
 */
public class UnorderedList extends HTMLElement{
    ObservableList<String> items;
    public UnorderedList(String initFontStyle) {
        super("ul", initFontStyle);
        items = FXCollections.observableArrayList();
    }
    
    public void addItem(String newItem){
        items.add(newItem);
    }
    public void removeItem(String itemToRemove){
        if (items.contains(itemToRemove))
            items.remove(itemToRemove);
    }
}
