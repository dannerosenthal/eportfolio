/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author Daniel
 */
public abstract class HTMLElement {
    String type;
    String innerHtml;
    String fontStyle;
    int fontSize;
    
    
    public void setInnerHtml(String html){
        innerHtml = html;
    }
    public String getInnerHtml(){
        return innerHtml;
    }
    
    public void setFontStyle(String font){
        fontStyle = font;
    }
    public String getFontStyle(){
        return fontStyle;
    }
    public String getType(){
        return type;
    }
    
    HTMLElement(String initType,String initFontStyle){
        type = initType;
        fontStyle = initFontStyle;
    }
    
    public String toHTML(){
        String html;
        html ="<" + type +">" + innerHtml + "</" + type +">";
        return html;
    }
}
