package data;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class ImageElement extends HTMLElement{
        String source;
    public ImageElement(String initFontStyle, String initSource) {
        super("img", initFontStyle);
        source = initSource;
    }
    public String getSource(){
        return source;
    }
    public void setSource(String initSource){
        source = initSource;
    }
}
