/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Daniel
 */
public class Website {
    ObservableList<Page> pages;
    String title;
    Page selectedPage;
    
    public Website(){
       pages = FXCollections.observableArrayList();
    }
    
    public void setTitle(String initTitle){
        title=initTitle;
    }
    public String getTitle(){
        return title;
    }
    public ObservableList getPages(){
        return pages;
    }
    public Page getSelectedPage(){
        return selectedPage;
    }
    
    public void addPage(Page pageToAdd){
        pages.add(pageToAdd);
    }
    public void removePage(){
        if (selectedPage!=null){
            pages.remove(selectedPage);
            selectedPage=null;
        }
    }
    public void setSelectedPage(Page selectPage){
        selectedPage = selectPage;
    }

    public void reset() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
