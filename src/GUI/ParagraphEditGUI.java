/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import data.Paragraph;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class ParagraphEditGUI extends HBox{
    TextArea textBox;
    Label label;
    VBox controls;
    Button hyperLinkButton;
    ComboBox fontChooser;
    Paragraph paragraph;
    
    ParagraphEditGUI(Paragraph initParagraph){
        label = new Label("Paragraph");
        label.setMinWidth(websiteEditGUI.LABEL_WIDTH);
        textBox = new TextArea();
        
        textBox.setWrapText(true);
        textBox.setPrefWidth(300);
        paragraph = initParagraph;
        textBox.setText(paragraph.getInnerHtml());
        
        textBox.setOnKeyReleased(e->{
        paragraph.setInnerHtml(textBox.getText());
        System.out.println(paragraph.getInnerHtml());
        });
        
        hyperLinkButton = new Button("Link");
        hyperLinkButton.setPrefWidth(websiteEditGUI.LABEL_WIDTH);
        hyperLinkButton.setOnAction(e ->{
            addHyperLink(); 
        });
        
        fontChooser = new ComboBox();
        fontChooser.getItems().addAll("Font 1","Font 2","Font 3");
        
        controls = new VBox();
        controls.setPrefWidth(websiteEditGUI.LABEL_WIDTH);
        controls.getChildren().addAll(label, hyperLinkButton, fontChooser);
        
        this.getChildren().addAll(controls, textBox);
    }

    private void addHyperLink() {
        Stage temp = new hyperLinkPopUp();
        temp.showAndWait();
        temp.close();
    }
      
}
