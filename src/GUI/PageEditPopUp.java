/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class PageEditPopUp extends Stage{
    Scene myScene;
    GridPane layout;
    Label pageTitle;
    Label bannerImage;
    Label pageFont;
    Label layoutType;
    Label colorScheme;
    
    TextField titleField;
    Button changeImageButton;
    Label bannerURL;
    ComboBox fontChooser;
    ComboBox layoutChooser;
    ComboBox colorChooser;
    PageEditPopUp(){
        layout = new GridPane();
        pageTitle = new Label("Page Title:");
        bannerImage = new Label("Banner Image");
        pageFont = new Label("Page Font");
        layoutType = new Label("Layout Type");
        colorScheme = new Label("Color Scheme");
        
        titleField = new TextField();
        changeImageButton = new Button("Change Banner");
        fontChooser = new ComboBox();
        fontChooser.getItems().addAll("Font 1","Font 2","Font 3");
        layoutChooser = new ComboBox();
        layoutChooser.getItems().addAll("layout 1", "layout 2", "layout 3");
        colorChooser = new ComboBox();
        colorChooser.getItems().addAll("scheme 1", "scheme 2" , "scheme3");
        
        layout.add(pageTitle,1,1);
        layout.add(titleField,2,1);
        
        layout.add(bannerImage,1,2);
        layout.add(changeImageButton, 2, 2);
        
        layout.add(pageFont,1,3);
        layout.add(fontChooser,2,3);
        
        layout.add(layoutType,1,4);
        layout.add(layoutChooser,2,4);
        
        layout.add(colorScheme,1,5);
        layout.add(colorChooser,2,5);
        
        myScene = new Scene(layout);
        this.setScene(myScene);
        
        
    }
}
