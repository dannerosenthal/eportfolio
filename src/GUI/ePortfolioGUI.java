/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import handlers.fileToolbarHandler;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class ePortfolioGUI {
    Stage myStage;
    Scene myScene;
    fileToolbarHandler ftHandler;
    
    BorderPane layout;
        HBox fileToolbar;
        WebView webWindow;
        websiteEditGUI editWindow;
    public WebView getWebWindow(){
        return webWindow;
    }
    public websiteEditGUI getEditWindow(){
        return editWindow;
    }
    public void newWebsite(){
        initializeUI();
        myStage.setScene(myScene);
    }
    
    public ePortfolioGUI(){
     initializeUI();

    }
    public void initializeUI(){
      fileToolbar = new HBox();
      editWindow = new websiteEditGUI();
      webWindow = new WebView();
      
      layout = new BorderPane();
      layout.setTop(fileToolbar);
      layout.setLeft(editWindow);
      layout.setCenter(webWindow);
      
              
      myScene= new Scene(layout);
      ftHandler = new fileToolbarHandler(this);
        initializeFileToolbar();
        try {
            initializeWebWindow();
        } catch (MalformedURLException ex) {
            Logger.getLogger(ePortfolioGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void startUI(Stage initPrimaryStage){
        
        //initializeFileToolbar();
        try {
            initializeWebWindow();
        } catch (MalformedURLException ex) {
            Logger.getLogger(ePortfolioGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        myStage = initPrimaryStage;
        myStage.setScene(myScene);
        myStage.show();
    }
    
    private void initializeFileToolbar(){
        Button newSiteButton = new Button();
        setButtonIcon(newSiteButton, "New.png","Create new website");
        
        Button loadSiteButton = new Button();
        setButtonIcon(loadSiteButton, "Load.png","Load a website");
        
        Button saveSiteButton = new Button();
        setButtonIcon(saveSiteButton, "Save.png","Save website");
        
        Button saveAsButton = new Button("SA");
        setButtonIcon(saveAsButton, "Save.png","Save As");
        
        Button exportButton = new Button();
        setButtonIcon(exportButton, "Export.png","Export Website");
        
        Button exitButton = new Button();
        setButtonIcon(exitButton, "Exit.png","Exit App");
        
        Button refreshWebViewButton = new Button("Refresh Preview");
        
        
        newSiteButton.setOnAction(e->{
            ftHandler.handleNewSiteRequest();
        });
        loadSiteButton.setOnAction(e->{
            ftHandler.handleLoadSiteRequest();
        });
        saveSiteButton.setOnAction(e->{
            ftHandler.handleSaveSiteRequest();
        });
        saveAsButton.setOnAction(e->{
            ftHandler.handleSaveAsRequest();
        });
        exportButton.setOnAction(e->{
            ftHandler.handleExportSiteRequest();
        });
        exitButton.setOnAction(e->{
            ftHandler.handleExitRequest();
        });
        refreshWebViewButton.setOnAction(e->{
            ftHandler.handleRefreshRequest();
        });
        fileToolbar.getChildren().addAll(newSiteButton, loadSiteButton, saveSiteButton,saveAsButton, exportButton, exitButton,refreshWebViewButton);
    }
    private void initializeWebWindow() throws MalformedURLException
    {
        File f = new File("./temp/index.html");
        webWindow.getEngine().load(f.toURI().toURL().toString());
        webWindow.setZoom(.5);
    }
     public void setButtonIcon(Button button,
	String iconFileName, String tooltip) {
	String imagePath = "file:"+"./img/icons/" + iconFileName;
	Image buttonImage = new Image(imagePath);
        button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(tooltip);
	button.setTooltip(buttonTooltip);
    }
}
