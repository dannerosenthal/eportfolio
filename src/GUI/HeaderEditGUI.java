/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import data.Header;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *
 * @author Daniel
 */
public class HeaderEditGUI extends HBox {
    TextField textBox;
    Label label;
    Header header;
    
    HeaderEditGUI(Header initHeader){
        label = new Label("Header");
        label.setMinWidth(websiteEditGUI.LABEL_WIDTH);
        textBox = new TextField();
        header = initHeader;
        textBox.setText(header.getInnerHtml());
          textBox.setOnKeyReleased(e->{
        header.setInnerHtml(textBox.getText());
        System.out.println(header.getInnerHtml());
        });
        
        this.getChildren().addAll(label, textBox);
    }
}
