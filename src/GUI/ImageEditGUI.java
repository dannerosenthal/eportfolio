/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import data.ImageElement;
import file.FileCopier;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class ImageEditGUI extends HBox {
    Label image;
    Label url;
    Button changeImageButton;
    ComboBox imagePosition;
    ImageElement imageElement;
    
    ImageEditGUI(ImageElement initImage){
        imageElement = initImage;
        image = new Label("Image");
        image.setMinWidth(websiteEditGUI.LABEL_WIDTH);
        url = new Label ("file/example.jpeg");
        changeImageButton = new Button("Change Image");
        imagePosition = new ComboBox();
        imagePosition.getItems().addAll("Align Left","Alight Right","Center");
        
        changeImageButton.setOnAction(e ->{
           chooseImage(); 
        });
        
        this.getChildren().addAll(image, changeImageButton, url, imagePosition);
        
    }
    
    private void chooseImage(){
        Stage tempStage = new Stage();
        FileChooser imageSelect = new FileChooser();
        File selectedFile = imageSelect.showOpenDialog(tempStage);
        
        String filePath = selectedFile.getAbsolutePath();
        String fileName = filePath.substring(filePath.lastIndexOf("\\")+1);
        System.out.println("Image Chosen: " + filePath);
        System.out.println("File name: " + fileName);
        
        File imageTarget = new File("temp/img/" + fileName);
        try {
            FileCopier.copyDirectory(selectedFile, imageTarget);

        } catch (IOException ex) {
            Logger.getLogger(ImageEditGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Copied File path:" + imageTarget.getAbsolutePath());
        imageElement.setSource(fileName);
        
    }
}
