/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class SlideShowEditPopUp extends Stage{


    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    Scene myScene;
    BorderPane ssmPane;

    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
    Button doneButton;
    
    // FOR THE SLIDE SHOW TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

   public SlideShowEditPopUp() {
        
        super();
        
        	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
        //workspace.getStyleClass().add("workspace");
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
	//slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = new Button("add Slide");
	removeSlideButton = new Button("remove Slide");
	moveSlideUpButton = new Button("Up");
	moveSlideDownButton = new Button("Down");
        
        doneButton = new Button("Done");
        
         addSlideButton.setOnAction(e -> {
	    addSlide();
         });
        slideEditToolbar.getChildren().addAll(addSlideButton, removeSlideButton, moveSlideUpButton, moveSlideDownButton);
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
	//initTitleControls();
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
        
         myScene = new Scene(workspace);
         this.setScene(myScene);
         this.setHeight(800);
         this.setWidth(1200);
    }
    /**
     * Initializes the UI controls and gets it rolling.
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
        private void addSlide() {
            slidesEditorPane.getChildren().add(new SlideEditView());
        }
}

