/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class SlideShowEditGUI extends HBox {
    Label slideShow;
    Button editSlideShowButton;
    
    SlideShowEditGUI(){
            slideShow = new Label("SlideShow");
            slideShow.setMinWidth(websiteEditGUI.LABEL_WIDTH);
            
        editSlideShowButton = new Button("Edit");
       
        
        editSlideShowButton.setOnAction(e ->{
           editSlideShow(); 
        });
        
        this.getChildren().addAll(slideShow,editSlideShowButton);
        
    }
    
    private void chooseImage(){
        Stage tempStage = new Stage();
        FileChooser imageSelect = new FileChooser();
        imageSelect.showOpenDialog(tempStage);
        tempStage.showAndWait();
        tempStage.close();
    }

    private void editSlideShow() {
       SlideShowEditPopUp temp = new SlideShowEditPopUp();
       temp.showAndWait();
       temp.close();
    }
}

