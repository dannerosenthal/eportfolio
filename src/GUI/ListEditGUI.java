/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 *
 * @author Daniel
 */
public class ListEditGUI extends HBox{
        Label list;
        Button addElementButton;
        ScrollPane elementPane;
        VBox elements;
        
        ListEditGUI(){
        list = new Label("List");
        list.setMinWidth(websiteEditGUI.LABEL_WIDTH);
        
        addElementButton = new Button("Add Element");
        addElementButton.setOnAction(e -> {
            addElement();
        });
        
        
        elementPane = new ScrollPane();
        elements = new VBox();
        elementPane.setContent(elements);
        
        this.getChildren().addAll(list, addElementButton,elementPane);
    }

    private void addElement() {
            elements.getChildren().add(new ListElementGUI());
    }
    
}
