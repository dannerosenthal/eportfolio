/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import data.HTMLElement;
import data.Header;
import data.ImageElement;
import data.Page;
import data.Paragraph;
import data.UnorderedList;
import data.Video;
import data.Website;
import handlers.pageHandlers;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class websiteEditGUI extends HBox {
    final int TOOLBAR_BUTTON_WIDTH = 200;
    final int COMPONENT_BUTTON_WIDTH = 125;
    final static int LABEL_WIDTH = 70;
    
    pageHandlers pHandler;
    Website website;
    
    VBox editToolbar;
    ScrollPane pageSelectPane;
        TextField webSiteTitle;
        VBox pageSelectToolbar;
        
    
        
        VBox componentEditor;
            HBox textToolbar;
            HBox mediaToolbar;
            ScrollPane componentPane; 
                VBox componentHolder;
    public Website getWebsite(){
        return website;
    }
    public websiteEditGUI(){
        pHandler = new pageHandlers(this);
        this.getChildren().clear();
        editToolbar = new VBox();
        componentEditor = new VBox();
            textToolbar = new HBox();
            mediaToolbar = new HBox();
        
        
        initializeEditToolbar();
        this.getChildren().addAll(editToolbar, componentEditor);
}
    private void initializeEditToolbar(){
        website = new Website();
        webSiteTitle = new TextField("Website Title(ex. your name)");
        //website.setTitle(webSiteTitle.getText());
        Button addPageButton = new Button("Add Page");
        Button removePageButton = new Button("Remove Page");
        Button editPageButton = new Button("Edit Page");
        addPageButton.setOnAction(e -> {
            addPage(new Page("Page","Font 1"));
        });
        editPageButton.setOnAction(e ->{
            editPage();
        });
        removePageButton.setOnAction(e ->{
            removeSelectedPage();
        });
        pageSelectToolbar = new VBox();
        pageSelectPane = new ScrollPane();
        pageSelectPane.setContent(pageSelectToolbar);
        editToolbar.getChildren().clear();
        editToolbar.getChildren().addAll(webSiteTitle, addPageButton,removePageButton,editPageButton,pageSelectPane);
        addPageButton.setMinWidth(TOOLBAR_BUTTON_WIDTH);
        removePageButton.setMinWidth(TOOLBAR_BUTTON_WIDTH);
        editPageButton.setMinWidth(TOOLBAR_BUTTON_WIDTH);
        
        webSiteTitle.setOnKeyReleased(e->{
            website.setTitle(webSiteTitle.getText());
        });
        
        
    }
    public void initializeEditPane(){
        Button addParagraphButton = new Button("Add Paragraph");
        Button addHeaderButton = new Button("Add Header");
        Button addListButton = new Button("Add List");
        
        addHeaderButton.setOnAction(e -> {
            Header headerToAdd = new Header("Font 1");
            addHeader(headerToAdd);
            website.getSelectedPage().addElement(headerToAdd);
        }
        );
        addParagraphButton.setOnAction(e -> {
            Paragraph paragraphToAdd = new Paragraph("Font 1");
            addParagraph(paragraphToAdd);
            website.getSelectedPage().addElement(paragraphToAdd);
        }
        );
        addListButton.setOnAction(e -> {
            addList();
        }
        );
        addParagraphButton.setMinWidth(COMPONENT_BUTTON_WIDTH);
        addListButton.setMinWidth(COMPONENT_BUTTON_WIDTH);
        addHeaderButton.setMinWidth(COMPONENT_BUTTON_WIDTH);
        textToolbar.getChildren().clear();
        textToolbar.getChildren().addAll(addParagraphButton,addHeaderButton,addListButton);
        
        Button addImageButton = new Button("Add Image");
        Button addSlideShowButton = new Button("Add SlideShow");
        Button addVideoButton = new Button("Add Video");
        
        addImageButton.setOnAction(e -> {
            ImageElement imageToAdd = new ImageElement("Font 1","");
            addImage(imageToAdd);
            website.getSelectedPage().addElement(imageToAdd);
        }
        );
        addVideoButton.setOnAction(e -> {
            Video videoToAdd = new Video("Font 1","");
            addVideo(videoToAdd);
            website.getSelectedPage().addElement(videoToAdd);
        }
        );
        addSlideShowButton.setOnAction(e -> {
            addSlideShow();
        }
        );
        addImageButton.setMinWidth(COMPONENT_BUTTON_WIDTH);
        addSlideShowButton.setMinWidth(COMPONENT_BUTTON_WIDTH);
        addVideoButton.setMinWidth(COMPONENT_BUTTON_WIDTH);
       
        mediaToolbar.getChildren().clear();
        mediaToolbar.getChildren().addAll(addImageButton,addSlideShowButton,addVideoButton);
        
        componentHolder = new VBox();
        componentPane = new ScrollPane(componentHolder);
        
        componentEditor.getChildren().clear();
        componentEditor.getChildren().addAll(textToolbar, mediaToolbar, componentPane);
        
        


    }
    private void addPage(Page pageToAdd){
        Button temp = new Button("Page");
        temp.setMinWidth(COMPONENT_BUTTON_WIDTH);
        temp.setMaxWidth(COMPONENT_BUTTON_WIDTH);
        pageSelectToolbar.getChildren().add(temp);
        
        //Page pageToAdd = new Page("Page","Font 1");
        website.addPage(pageToAdd);
        website.setSelectedPage(pageToAdd);
        initializeEditPane();
        
        temp.setOnAction(e->{
            //website.setSelectedPage(pageToAdd);
            pHandler.selectPageHandler(pageToAdd);
        });
    }
    private void removeSelectedPage(){
        website.removePage();
        loadWebsiteFromFile(website);
    }
    private void addHeader(Header headerToAdd){
       //Header headerToAdd = new Header("Font 1");
       //website.getSelectedPage().addElement(headerToAdd);
       componentHolder.getChildren().add(new HeaderEditGUI(headerToAdd));
    }

    private void addParagraph(Paragraph paragraphToAdd) {
       //Paragraph paragraphToAdd = new Paragraph("Font 1");
       componentHolder.getChildren().add(new ParagraphEditGUI(paragraphToAdd));
    }

    private void addList() {
        componentHolder.getChildren().add(new ListEditGUI());
    }

    private void addImage(ImageElement imageToAdd) {
       componentHolder.getChildren().add(new ImageEditGUI(imageToAdd));
    }

    private void addVideo(Video videoToAdd) {
       componentHolder.getChildren().add(new VideoEditGUI(videoToAdd));
    }

    private void addSlideShow() {
        componentHolder.getChildren().add(new SlideShowEditGUI());
    }

    private void editPage() {
        Stage temp = new PageEditPopUp();
        temp.showAndWait();
        temp.close();
    }

    public void loadSelectedPage() {
        if(website.getSelectedPage()!=null){
            Page pageToLoad = website.getSelectedPage();
            for (HTMLElement element : (ObservableList<HTMLElement>) pageToLoad.getElements()) {
                switch(element.getType()){
                case("p"):
                    addParagraph((Paragraph) element);
                break;
                case("h2"):
                    addHeader((Header) element);
                break;
                case("img"):
                    //addImage((ImageElement) element);
                break;
                case("video"):
                    //addVideo((Video) element);
                break;
                case("ul"):
                    //addList((UnorderedList) element);
                break;
            }
        }
        }
    }
    public void loadWebsiteFromFile(Website websiteToLoad){
        initializeEditToolbar();
        initializeEditPane();
    
        for(Page page: (ObservableList<Page>)websiteToLoad.getPages()){
            addPage(page);
            website.setSelectedPage(page);
            loadSelectedPage();
        }
        website.setTitle(websiteToLoad.getTitle());
        webSiteTitle.setText(website.getTitle());
        
    }
}
