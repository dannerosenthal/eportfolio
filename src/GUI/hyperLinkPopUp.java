/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class hyperLinkPopUp extends Stage{
    Label hyperlink;
    TextField linkField;
    Button okButton;
    Scene myScene;
    HBox layout;
    
    hyperLinkPopUp(){
        super();
        hyperlink = new Label("insert link URL:");
        linkField = new TextField();
        okButton = new Button("OK");
        
        layout = new HBox();
        layout.getChildren().addAll(hyperlink,linkField,okButton);
        
        myScene=new Scene(layout);
        this.setScene(myScene);
    }
}
