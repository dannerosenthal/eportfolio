/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 *
 * @author Daniel
 */
public class ListElementGUI extends HBox{
    TextField text;
    Button deleteButton;
    ListElementGUI(){
        text = new TextField();
        deleteButton = new Button("Delete");
        
        this.getChildren().addAll(text, deleteButton);
    }
    
}
