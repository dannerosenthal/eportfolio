/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import data.Video;
import file.FileCopier;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Daniel
 */
public class VideoEditGUI extends HBox {

    Label video;
    Label url;
    Button changeVideoButton;
    ComboBox videoSize;
    Video videoElement;
    
    VideoEditGUI(Video initVideo){
        
        videoElement = initVideo;
        
        video = new Label("Video");
        video.setMinWidth(websiteEditGUI.LABEL_WIDTH);
        
        url = new Label ("file/example.mp4");
        changeVideoButton = new Button("Change Video");
        videoSize = new ComboBox();
        videoSize.getItems().addAll("small","medium","large");
        
        changeVideoButton.setOnAction(e ->{
           chooseVideo(); 
        });
        
        this.getChildren().addAll(video, changeVideoButton, url, videoSize);
        
    }
    
    private void chooseVideo(){
        Stage tempStage = new Stage();
        FileChooser videoSelect = new FileChooser();
        File selectedFile = videoSelect.showOpenDialog(tempStage);
        
        String filePath = selectedFile.getAbsolutePath();
        String fileName = filePath.substring(filePath.lastIndexOf("\\")+1);
        System.out.println("Video Chosen: " + filePath);
        System.out.println("File name: " + fileName);
        
        File videoTarget = new File("temp/img/" + fileName);
        try {
            FileCopier.copyDirectory(selectedFile, videoTarget);

        } catch (IOException ex) {
            Logger.getLogger(ImageEditGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Copied File path:" + videoTarget.getAbsolutePath());
        videoElement.setSource(fileName);
        

    }
}
