/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import data.Page;
import data.Paragraph;
import data.Website;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel
 */
public class FileTester {
  public static void main(String[] args){
      Website myWebsite = new Website();
      myWebsite.setTitle("My Website");
      myWebsite.addPage(new Page("Page 1","Cool Font"));
      Page myPage = (Page) myWebsite.getPages().get(0);
      myPage.addElement(new Paragraph("Cool Font 2"));
      
      
      WebsiteFileManager fileManager = new WebsiteFileManager();
      try {
          fileManager.saveWebsite(myWebsite);
      } catch (IOException ex) {
          Logger.getLogger(FileTester.class.getName()).log(Level.SEVERE, null, ex);
      }
      myWebsite=new Website();
      try {
          myWebsite=fileManager.loadWebsite(new File("./data/"+"My Website"+".json"));
      } catch (IOException ex) {
          Logger.getLogger(FileTester.class.getName()).log(Level.SEVERE, null, ex);
      }
       try {
          fileManager.saveWebsite(myWebsite);
      } catch (IOException ex) {
          Logger.getLogger(FileTester.class.getName()).log(Level.SEVERE, null, ex);
      }
  }  
}
