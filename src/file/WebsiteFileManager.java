/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import data.Page;
import data.HTMLElement;
import data.Header;
import data.ImageElement;
import data.Paragraph;
import data.UnorderedList;
import data.Video;
import data.Website;
import java.io.File;
import java.math.BigDecimal;
import javafx.collections.ObservableList;

/**
 * This class uses the JSON standard to read and write slideshow data files.
 *
 * @author McKilla Gorilla & _____________
 */
public class WebsiteFileManager {

    // JSON FILE READING AND WRITING CONSTANTS

    public static String JSON_TITLE = "title";
    public static String JSON_ELEMENTS = "elements";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String PATH_WEBSITE = "./data";
    public static String JSON_PAGES = "pages";
    public static String JSON_FONT = "font";

    /**
     * This method saves all the data associated with a webpage to a JSON
     * file.
     *
     * @param pageToSave The page whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void saveWebsite(Website websiteToSave) throws IOException {
	StringWriter sw = new StringWriter();

	// BUILD THE SLIDES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(websiteToSave.getPages());

	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject pageJsonObject = Json.createObjectBuilder()
		.add(JSON_TITLE, websiteToSave.getTitle())
		.add(JSON_PAGES, pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(pageJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String websiteTitle = "" + websiteToSave.getTitle();
	String jsonFilePath = PATH_WEBSITE + SLASH + websiteTitle + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(pageJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideSShowModel object.
     *
     * @param slideShowToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
    public Website loadWebsite(File jsonFilePath) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
        Website websiteToLoad = new Website();
	JsonObject json = loadJSONFile(jsonFilePath);

	// NOW LOAD THE COURSE
        Page pageToAdd;
	//websiteToLoad.reset();
	websiteToLoad.setTitle(json.getString(JSON_TITLE));
	JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
	for (int i = 0; i < jsonPagesArray.size(); i++) {
	    JsonObject pageJso = jsonPagesArray.getJsonObject(i);
            pageToAdd = loadPageFromJSONFile(pageJso);
	    websiteToLoad.addPage(pageToAdd);
	}
        return websiteToLoad;
    }
    

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(File jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(File jsonFilePath, String arrayName) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);
	ArrayList<String> items = new ArrayList();
	JsonArray jsonArray = json.getJsonArray(arrayName);
	for (JsonValue jsV : jsonArray) {
	    items.add(jsV.toString());
	}
	return items;
    }
    private Page loadPageFromJSONFile(JsonObject jso){
        Page pageToLoad = new Page(jso.getString(JSON_TITLE),jso.getString(JSON_FONT));
        JsonArray jsonElementsArray = jso.getJsonArray(JSON_ELEMENTS);
        HTMLElement elementToLoad = null;
        for (int i = 0; i < jsonElementsArray.size(); i++) {
	    JsonObject elementJso = jsonElementsArray.getJsonObject(i);
            switch(elementJso.getString("type")){
                case("p"):
                    pageToLoad.addElement(loadParagraphFromJsonElement(elementJso));
                break;
                case("h2"):
                    pageToLoad.addElement(loadHeaderFromJsonElement(elementJso));
                break;
                case("img"):
                    pageToLoad.addElement(loadImageFromJsonElement(elementJso));
                break;
                case("video"):
                    pageToLoad.addElement(loadVideoFromJsonElement(elementJso));
                break;
                case("ul"):
                    pageToLoad.addElement(loadListFromJsonElement(elementJso));
                break;
            }
            
	}
        
        
        return pageToLoad;
    }
    private JsonObject makePageJsonObject(Page page){
         //need to add page font
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_TITLE, page.getTitle())
                .add(JSON_FONT, page.getFont())
                .add(JSON_ELEMENTS, makeElementsJsonArray(page.getElements()))
		.build();
	return jso;
    }
    private JsonArray makePagesJsonArray(List<Page> pages) {
       JsonArrayBuilder jsb = Json.createArrayBuilder();
       for (Page page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
	} 
       
       JsonArray jA = jsb.build();
       return jA;
    }
    private JsonArray makeElementsJsonArray(List<HTMLElement> elements){
         JsonArrayBuilder jsb = Json.createArrayBuilder();
         JsonObject jso = null;
       for (HTMLElement element : elements) {
	    switch(element.getType()){
                case("p"):
                    jso = makeParagraphJsonObject((Paragraph) element);
                break;
                case("h2"):
                    jso = makeHeaderJsonObject((Header) element);
                break;
                case("img"):
                    jso = makeImageJsonObject((ImageElement) element);
                break;
                case("video"):
                    jso = makeVideoJsonObject((Video) element);
                break;
                case("ul"):
                    jso = makeUnorderedListJsonObject((UnorderedList) element);
                break;
            }
	    jsb.add(jso);
	} 
       
       JsonArray jA = jsb.build();
       return jA;
    }

    private JsonObject makeParagraphJsonObject(Paragraph paragraph) {
         JsonObject jso = Json.createObjectBuilder()
                .add("type", "p")
                .add("text", paragraph.getInnerHtml())
                .add("font", paragraph.getFontStyle())
		.build();
	return jso;
    }

    private JsonObject makeHeaderJsonObject(Header header) {
        JsonObject jso = Json.createObjectBuilder()
                .add("type", "h2")
                .add("text", header.getInnerHtml())
                .add("font", header.getFontStyle())
		.build();
	return jso;
    }

    private JsonObject makeImageJsonObject(ImageElement image) {
       JsonObject jso = Json.createObjectBuilder()
                .add("type", "img")
                .add("source", image.getSource())
                .add("font", image.getFontStyle())

		.build();
	return jso;
    }

    private JsonObject makeVideoJsonObject(Video video) {
        JsonObject jso = Json.createObjectBuilder()
                .add("type", "video")
                .add("source", video.getSource())
                .add("font", video.getFontStyle())
		.build();
	return jso;
    }

    private JsonObject makeUnorderedListJsonObject(UnorderedList unorderedList) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
    private Paragraph loadParagraphFromJsonElement(JsonObject elementJso) {
       Paragraph paragraph;
       paragraph = new Paragraph(elementJso.getString("font"));
       paragraph.setInnerHtml(elementJso.getString("text"));
       return paragraph;
    }

    private Header loadHeaderFromJsonElement(JsonObject elementJso) {
       Header header = new Header(elementJso.getString("font"));
       header.setInnerHtml(elementJso.getString("text"));
       return header;
    }

    private ImageElement loadImageFromJsonElement(JsonObject elementJso) {
        ImageElement image = new ImageElement(elementJso.getString("font"),elementJso.getString("source"));
        return image;
    }

    private HTMLElement loadVideoFromJsonElement(JsonObject elementJso) {
      Video video = new Video(elementJso.getString("font"),elementJso.getString("source"));
      return video;
    }

    private HTMLElement loadListFromJsonElement(JsonObject elementJso) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
