/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handlers;

import GUI.ePortfolioGUI;
import GUI.websiteEditGUI;
import data.Website;
import file.WebsiteFileManager;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import file.FileCopier;

/**
 *
 * @author Daniel
 */
public class fileToolbarHandler {
    ePortfolioGUI ui;
    
    public fileToolbarHandler(ePortfolioGUI ui){
        this.ui = ui;
    }

    public void handleNewSiteRequest() {
        ui.newWebsite();   
    }

    public void handleLoadSiteRequest(){
        WebsiteFileManager loader = new WebsiteFileManager();
        Website websiteToLoad;
            try {
                Stage tempStage = new Stage();
                FileChooser websiteSelect = new FileChooser();
                websiteSelect.setInitialDirectory(new File("./data"));
                websiteSelect.getExtensionFilters().add(new ExtensionFilter("Website", "*.JSON"));
                File selectedFile = websiteSelect.showOpenDialog(tempStage);
                websiteToLoad = loader.loadWebsite(selectedFile);
                //tempStage.showAndWait();
                //tempStage.close();
                ui.getEditWindow().loadWebsiteFromFile(websiteToLoad);
            } catch (IOException ex) {
                Logger.getLogger(fileToolbarHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    public void handleSaveSiteRequest() {
        WebsiteFileManager saver = new WebsiteFileManager();
        Website websiteToSave=ui.getEditWindow().getWebsite();
        if(websiteToSave!=null){
            try {
                saver.saveWebsite(websiteToSave);
            } catch (IOException ex) {
                Logger.getLogger(fileToolbarHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void handleSaveAsRequest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void handleExportSiteRequest(){
        handleRefreshRequest();
        try {
            FileCopier.copyDirectory(new File("./temp"),
                    new File("./data/sites/" + ui.getEditWindow().getWebsite().getTitle()));
        } catch (IOException ex) {
            Logger.getLogger(fileToolbarHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void handleExitRequest() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void handleRefreshRequest() {
        handleSaveSiteRequest();
        String websiteTitle = ui.getEditWindow().getWebsite().getTitle();
        for(int i = 1; i<ui.getEditWindow().getWebsite().getPages().size(); i++){
            try {
            FileCopier.copyDirectory(new File("./temp/index.html"),
                    new File("./temp/index"+i+".html"));
            } catch (IOException ex) {
            Logger.getLogger(fileToolbarHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        File source = new File ("./data/" + websiteTitle + ".json");
        try {
            FileCopier.copyDirectory(source,
                    new File("./temp/js/TEST JSON.json"));
        } catch (IOException ex) {
            Logger.getLogger(fileToolbarHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        ui.getWebWindow().getEngine().reload();
    }
    
    
}
