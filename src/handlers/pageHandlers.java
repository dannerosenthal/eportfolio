/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package handlers;

import GUI.ePortfolioGUI;
import GUI.websiteEditGUI;
import data.Page;
/**
 *
 * @author Daniel
 */
public class pageHandlers {
    websiteEditGUI ui;
    
    public pageHandlers(websiteEditGUI gui){
        ui = gui;
    }
    
    public void selectPageHandler(Page pageToSelect){
        ui.initializeEditPane();
        ui.getWebsite().setSelectedPage(pageToSelect);
        ui.loadSelectedPage();
    }
}
